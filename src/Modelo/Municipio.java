package Modelo;

import ufps.util.colecciones_seed.ColaP;
import ufps.util.colecciones_seed.ListaCD;

public class Municipio {

    private int id_municipio;

    private String nombre;

    private ColaP<Persona> personas = new ColaP();

    public Municipio() {
    }

    public Municipio(int id_municipio, String nombre) {
        this.id_municipio = id_municipio;
        this.nombre = nombre;
    }

    public int getId_municipio() {
        return id_municipio;
    }

    public void setId_municipio(int id_municipio) {
        this.id_municipio = id_municipio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ColaP<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ColaP<Persona> personas) {
        this.personas = personas;
    }

    @Override
    public String toString() {
        String msg = "\nMunicipio:" + this.getNombre();
        ColaP<Persona> temp = new ColaP();
        while (!this.personas.esVacia()) {
            Persona p = this.personas.deColar();
            msg += "Persona:" + p.toString() + "\n";
            temp.enColar(p, p.getPrioridad());
        }
        this.personas = temp;
        return msg;
    }

    public void insertarPersona(Persona nueva) {
        this.personas.enColar(nueva, nueva.getPrioridad());
    }

    public ListaCD<Persona> getListadoPersonas() {
        ListaCD<Persona> l = new ListaCD();
        ColaP<Persona> temp = new ColaP();
        while (!this.personas.esVacia()) {
            Persona p = this.personas.deColar();
            l.insertarAlFinal(p);
            temp.enColar(p, p.getPrioridad());
        }
        this.personas = temp;
        return l;
    }

}
