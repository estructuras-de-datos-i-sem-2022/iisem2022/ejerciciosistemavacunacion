/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.*;
import java.time.LocalDateTime;
import java.time.Month;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madarme
 */
public class SistemaNacionalVacunacion {

    private int etapa;
    private String urlDpto, urlMunicipio, urlPersona, urlContenedor;
    private final ListaCD<Departamento> dptos = new ListaCD();
    private final ListaCD<NotificacionVacunado> registros = new ListaCD();
    private Proveedor[] proveedores;

    public SistemaNacionalVacunacion(String urlDpto, String urlMunicipio, String urlPersona, String urlContenedor) {
        this.urlDpto = urlDpto;
        this.urlMunicipio = urlMunicipio;
        this.urlPersona = urlPersona;
        this.urlContenedor = urlContenedor;
        this.crearDpto();
        this.crearMunicipios();
        this.crearPersonas();
    }

    private void crearDpto() {
        ArchivoLeerURL dpto = new ArchivoLeerURL(this.urlDpto);
        Object datos[] = dpto.leerArchivo();
        //id_dpto;Nombre Departamento
        for (int i = 1; i < datos.length; i++) {
            Object dato_dpto = datos[i];
            //Cómo está en un object pasarlo a String y split
            String datosD[] = dato_dpto.toString().split(";");
            Departamento nuevo = new Departamento(Integer.parseInt(datosD[0]), datosD[1]);
            this.dptos.insertarAlFinal(nuevo);
        }
    }

    private void crearMunicipios() {
        ArchivoLeerURL d = new ArchivoLeerURL(this.urlMunicipio);
        Object datos[] = d.leerArchivo();
        //id_dpto;id_municipio;nombreMunicipio
        for (int i = 1; i < datos.length; i++) {
            Object dato_dpto = datos[i];
            //Cómo está en un object pasarlo a String y split
            String datosD[] = dato_dpto.toString().split(";");
            Municipio m = new Municipio(Integer.parseInt(datosD[1]), datosD[2]);
            Departamento dd = this.getDpto(Integer.parseInt(datosD[0]));
            if (dd == null) {
                throw new RuntimeException("Dpto no existe");
            }
            dd.addMunicipio(m);
        }
    }

    private void crearPersonas() {
        ArchivoLeerURL d = new ArchivoLeerURL(this.urlPersona);
        Object datos[] = d.leerArchivo();
        for (int i = 1; i < datos.length; i++) {
            String datosP[] = datos[i].toString().split(";");
            //cedula;nombre;fechanacimiento;id_municipio_inscripcion;email
            int cc = Integer.parseInt(datosP[0]);
            //partir la fecha AA-MM-DD
            String fecha[] = datosP[2].split("-");
            LocalDateTime fechaN = LocalDateTime.of(Integer.parseInt(fecha[0]), Integer.parseInt(fecha[1]), Integer.parseInt(fecha[2]), 0, 0);
            String nombre = datosP[1];
            int idMun = Integer.parseInt(datosP[3]);
            String email = datosP[4];
            Persona nueva = new Persona(cc, nombre, fechaN, email);
            Municipio m=this.getMunicipio(idMun);
            if(m==null)
                throw new RuntimeException("Municipio no existe");
            m.insertarPersona(nueva);
        }
    }

    private Departamento getDpto(int idDpto) {
        for (Departamento d : this.dptos) {
            if (d.getId_dpto() == idDpto) {
                return d;
            }
        }
        return null;
    }

    private Municipio getMunicipio(int idMun) {
        for (Departamento d : this.dptos) {
            Municipio m = d.getMunicipio(idMun);
            if (m != null) {
                return m;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String msg = "SistemaNacionalVacunacion:\nListado Dptos:\n";
        for (Departamento d : this.dptos) {
            msg += d.toString() + "\n";
            msg += d.getMunicipios().getTamanio() + "\n";
        }
        return msg;
    }

    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    
    
}
