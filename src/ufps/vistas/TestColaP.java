/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ColaP;

/**
 *
 * @author estudiante
 */
public class TestColaP {

    public static void main(String[] args) {
        ColaP<String> nombres = new ColaP();
        nombres.enColar("Shirley", -100);
        nombres.enColar("Mauricio", -200);
        nombres.enColar("Ana", -150);
        while (!nombres.esVacia()) {
            System.out.println(nombres.deColar());
        }

    }

}
