/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ufps.vistas;

import Modelo.*;
import Negocio.SistemaNacionalVacunacion;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;

import java.io.FileOutputStream;
import java.time.Instant;
import java.util.Date;

/**
 *
 * @author madar
 */
public class PDF_SistemaVacuna {

    SistemaNacionalVacunacion s;

    public PDF_SistemaVacuna() {
    }

    public PDF_SistemaVacuna(SistemaNacionalVacunacion s) {
        this.s = s;
    }

    public String crearPDF() {
        Date fecha = Date.from(Instant.now());
        String nombreArchivo = "src/pdf/resultado-" + fecha.getTime() + ".pdf";
        File f=new File(nombreArchivo);
        Document document = new Document();
        Font fuente = new Font();
        fuente.setSize(20);
        Paragraph titulo = new Paragraph("Listado Sistema Vacunación \n\n", fuente);
        titulo.setAlignment(Element.ALIGN_CENTER);
        try {
            FileOutputStream archiPDF = new FileOutputStream(nombreArchivo);
            PdfWriter w = PdfWriter.getInstance(document, archiPDF);
            MembreteHeaderiText header = new MembreteHeaderiText();
            //Asignamos el manejador de eventos al escritor para paginar y colocar encabezados
            w.setPageEvent(header);
            document.open();
            document.add(titulo);
            document.add(this.crearDpto_Mun());
            document.close();

        } catch (Exception e) {
            System.err.println("Ocurrio un error al crear el archivo");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
        return f.getAbsolutePath();
    }

    private PdfPTable crearDpto_Mun() {
        PdfPTable table = new PdfPTable(3);
        table.addCell("Departamento");
        table.addCell("Municipio");
        table.addCell("Personas");
        for (Departamento d : this.s.getDptos()) {
            PdfPCell celdaFinal = new PdfPCell(new Paragraph(d.getNombreDpto()));
            celdaFinal.setRowspan(d.getMunicipios().getTamanio());
            table.addCell(celdaFinal);
            for (Municipio m : d.getMunicipios()) {
                table.addCell(m.getNombre());
                table.addCell(this.getTablaPersonas(m));
            }

        }
        return table;
    }

    private PdfPTable getTablaPersonas(Municipio m) {
        PdfPTable table = new PdfPTable(4);
        Font fuente = new Font();
        fuente.setSize(5);
        table.addCell(new Paragraph("CC", fuente));
        table.addCell(new Paragraph("Nombre", fuente));
        table.addCell(new Paragraph("Email", fuente));
        table.addCell(new Paragraph("Fecha Nacimiento", fuente));
        if (m.getListadoPersonas().getTamanio() == 0) {
            table.addCell("--");
            table.addCell("--");
            table.addCell("--");
            table.addCell("--");
        } else {
            for (Persona p : m.getListadoPersonas()) {
                table.addCell(new Paragraph(p.getCedula() + "", fuente));
                table.addCell(new Paragraph(p.getNombre(), fuente));
                table.addCell(new Paragraph(p.getEmail(), fuente));
                table.addCell(new Paragraph(p.getFechaNacimiento().toString(), fuente));
            }
        }
        return table;
    }

    public class MembreteHeaderiText extends PdfPageEventHelper {

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("Estructuras de Datos-Ing Sistemas UFPS:\t Página(" + writer.getPageNumber()+")"), 200, 830, 0);
        }
    }
}
