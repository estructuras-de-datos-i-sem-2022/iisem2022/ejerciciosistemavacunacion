/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import Negocio.SistemaNacionalVacunacion;

/**
 *
 * @author madar
 */
public class TestSistemaVacunacion {
    
    public static void main(String[] args) {
        
        String urlDpto="https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/departamento.csv";
        String urlMun="https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/municipios.csv";
        String urlPer="https://gitlab.com/madarme/archivos-persistencia/-/raw/master/votaciones/personas.csv";
        SistemaNacionalVacunacion s=new SistemaNacionalVacunacion(urlDpto, urlMun, urlPer,"");
        //System.out.println(s.toString());
        PDF_SistemaVacuna pdf=new PDF_SistemaVacuna(s);
        System.out.println("Archivo creado en:"+pdf.crearPDF());
    }
    
}
